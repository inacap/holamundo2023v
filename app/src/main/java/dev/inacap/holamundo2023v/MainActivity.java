package dev.inacap.holamundo2023v;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import dev.inacap.holamundo2023v.vistas.HomeActivity;
import dev.inacap.holamundo2023v.vistas.RegistrarActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Abro comunicacion con Firebase
        FirebaseApp.initializeApp(this);

        // Abro servicio de autenticacion
        FirebaseAuth mAuth = FirebaseAuth.getInstance();

        // Inicio alguien sesion?
        FirebaseUser usuario = mAuth.getCurrentUser();
        if(usuario != null){
            // Nos movemos inmediatamente al HomeActivity
            Intent i = new Intent(MainActivity.this, HomeActivity.class);
            startActivity(i);
            finish();
            return;
        }

        TextView tvCrear = findViewById(R.id.tv_crear_cuenta);
        Button btIniciar = findViewById(R.id.btn_login);
        btIniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText campoEmail = findViewById(R.id.et_username);
                EditText campoPass = findViewById(R.id.et_pass1);

                String email = campoEmail.getText().toString().trim();
                String pass = campoPass.getText().toString().trim();

                // TODO: validacion previa

                mAuth.signInWithEmailAndPassword(email, pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Intent i = new Intent(MainActivity.this, HomeActivity.class);
                            startActivity(i);
                            finish();
                        } else {
                            Toast.makeText(MainActivity.this, "Usuario o pass incorrectos", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        tvCrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, RegistrarActivity.class);
                startActivity(i);
            }
        });
    }
}