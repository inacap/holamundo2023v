package dev.inacap.holamundo2023v.modelos;

public class Mascota {
    public String nombre;
    public String tipo;
    public String dueno;

    public Mascota() {
    }

    public Mascota(String nombre, String tipo, String dueno) {
        this.nombre = nombre;
        this.tipo = tipo;
        this.dueno = dueno;
    }
}
