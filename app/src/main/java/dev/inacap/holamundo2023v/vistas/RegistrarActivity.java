package dev.inacap.holamundo2023v.vistas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import dev.inacap.holamundo2023v.R;

public class RegistrarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);

        TextView cancelar = findViewById(R.id.tv_cancelar);
        Button btGuardar = findViewById(R.id.btn_guardar);

        // Iniciar una app Firebase
        FirebaseApp.initializeApp(this);

        FirebaseAuth mAuth = FirebaseAuth.getInstance();

        btGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText campoUsuario = findViewById(R.id.et_username);
                EditText campoPass1 = findViewById(R.id.et_pass1);
                EditText campoPass2 = findViewById(R.id.et_pass2);

                String usuario = campoUsuario.getText().toString().trim();
                String pass1 = campoPass1.getText().toString().trim();
                String pass2 = campoPass2.getText().toString().trim();

                // Validacion de los datos
                if(usuario == null || usuario.equals("")){
                    campoUsuario.setError("Campo obligatorio");
                    return;
                }

                if(pass1 == null || pass1.equals("")){
                    campoPass1.setError("Campo obligatorio");
                    return;
                }

                if(pass1.length() < 6){
                    campoPass1.setError("Debe tener minimo 6 caracteres.");
                    return;
                }

                if(!pass1.equals(pass2)){
                    campoPass2.setError("Las contraseñas no coinciden");
                    return;
                }

                // Crear cuenta de usuario
                mAuth.createUserWithEmailAndPassword(usuario, pass1).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(RegistrarActivity.this, "Usuario creado!", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(RegistrarActivity.this, "No se pudo crear el usuario", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}