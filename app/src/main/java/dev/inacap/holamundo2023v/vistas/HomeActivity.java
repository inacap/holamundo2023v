package dev.inacap.holamundo2023v.vistas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.widget.AppCompatImageButton;
import dev.inacap.holamundo2023v.MainActivity;
import dev.inacap.holamundo2023v.R;
import dev.inacap.holamundo2023v.modelos.Mascota;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // Establecer comunicacion con Firebase
        FirebaseApp.initializeApp(this);
        FirebaseAuth mAuth = FirebaseAuth.getInstance();

        // Conecto al servicio de Firestore
        FirebaseFirestore mStore = FirebaseFirestore.getInstance();

        // Obtener usuario que inicio sesion
        FirebaseUser usuario = mAuth.getCurrentUser();

        TextView tvEmail = findViewById(R.id.tv_email);
        tvEmail.setText(usuario.getEmail());

        Button btSalir = findViewById(R.id.bt_salir);
        btSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Cerrar sesion en Firebase
                mAuth.signOut();
                Toast.makeText(HomeActivity.this, "Hasta pronto", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(HomeActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        });

        Button btGuardar = findViewById(R.id.bt_guardar);
        btGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText etNombre = findViewById(R.id.et_nombre);
                String nombre = etNombre.getText().toString().trim();
                String dueno = usuario.getEmail();

                // Creo la mascota
                Mascota nuevaMascota = new Mascota(nombre, "", dueno);

                // Guardar en firestore
                mStore.collection("mascotas").add(nuevaMascota);
                Toast.makeText(HomeActivity.this, "Mascota guardada!", Toast.LENGTH_SHORT).show();
            }
        });

        ImageButton btActualizar = findViewById(R.id.bt_actualizar);
        btActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Pedir datos a FireStore
                mStore.collection("mascotas").whereEqualTo("dueno", usuario.getEmail())
                        .get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                            @Override
                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                                List<DocumentSnapshot> listaDocumentos = queryDocumentSnapshots.getDocuments();
                                List<String> listaSimpleMascotas = new ArrayList<String>();

                                for(int contador = 0; contador < listaDocumentos.size(); contador++){
                                    Mascota mas = listaDocumentos.get(contador).toObject(Mascota.class);
                                    Log.i("MASCOTA", "Nombre: " + mas.nombre);
                                    listaSimpleMascotas.add(mas.nombre);
                                }

                                ArrayAdapter<String> adaptador = new ArrayAdapter<String>(
                                        HomeActivity.this,
                                        android.R.layout.simple_list_item_1,
                                        listaSimpleMascotas
                                );

                                ListView lvMascotas = findViewById(R.id.lv_mascotas);
                                lvMascotas.setAdapter(adaptador);
                            }
                        });
            }
        });
    }
}







